/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.watcharaphon.finalproject;

import java.io.Serializable;

/**
 *
 * @author Lenovo
 */
public class Stock implements Serializable {
    private String name;
    private int num;

    @Override
    public String toString() {
        return "Stock{" + "name=" + name + ", num=" + num + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Stock(String name, int num) {
        this.name = name;
        this.num = num;
    }
}
